var gulp = require('gulp'),
    newer = require('gulp-newer'),
    concat = require('gulp-concat'),
    less = require('gulp-less'),
    htmlmin = require('gulp-htmlmin'),
    uglify = require('gulp-uglify'),
    gutil = require('gulp-util'),
    size = require('gulp-size'),
    inject = require('gulp-inject'),
    // autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

gulp.task('fonts', function () {
    return gulp.src('./src/fonts/**/*')
        .pipe(gulp.dest('./dist/fonts'))
        .pipe(reload({stream:true}))
        .on('error', gutil.log);
});

gulp.task('css', function () {
    return gulp.src([
        './node_modules/bootstrap/dist/css/bootstrap.css',
        // './node_modules/font-awesome/css/font-awesome.css',
        './src/css/main.css'])
        .pipe(newer('dist/css/app.css'))
        .pipe(concat('app.css'))
        .pipe(size())
        .pipe(cleanCSS())
        .pipe(gulp.dest('dist/css'))
        .pipe(size())
        .pipe(reload({stream:true}))
        .on('error', gutil.log);
});

gulp.task('index', ['css'], function() {
    return gulp.src('./src/index.html')
        .pipe(inject(gulp.src('./dist/css/app.css', {read: false}), {name: 'styles', addRootSlash: false, ignorePath: 'dist/'}))
        .pipe(inject(gulp.src('./dist/js/app.js', {read: false}), {name: 'scripts', addRootSlash: false, ignorePath: 'dist/'}))
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('./dist'))
        .pipe(reload({stream:true}));
});

gulp.task('json', function() {
    return gulp.src('./src/data/**/*.json')
        .pipe(gulp.dest('./dist/data'));
});

gulp.task('php', function() {
    return gulp.src('./src/php/**/*.php')
        .pipe(gulp.dest('./dist/php'));
});

gulp.task('js', function () {
    return gulp.src([
        './node_modules/jquery/dist/jquery.js',
        './src/app.js',
        './src/js/**/*.js'
    ])
        .pipe(newer('dist/js/app.js'))
        .pipe(concat('app.js'))
        .pipe(size())
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'))
        .pipe(size())
        .pipe(reload({stream:true}));
});

gulp.task('watch', function() {
    gulp.watch(['src/js/**/*.js', 'src/app.js'], ['js']);
    gulp.watch('./src/index.html', ['index']);
    gulp.watch('./src/css/**/*.css', ['css']);
    gulp.watch('./src/data/**/*.json', ['json']);
    gulp.watch('./src/php/**/*.php', ['php']);
});

gulp.task('browser-sync', ['watch'], function () {
    var files = [
        './dist/**/*.*'
    ];

    browserSync.init(files, {
        // proxy: 'localhost:8000'
        server: {
            baseDir: './dist/'
        }
    });
});

gulp.task('default', ['index', 'fonts', 'json', 'php', 'js', 'browser-sync']);