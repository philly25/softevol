<?php
$files = array();
$path = $_SERVER['DOCUMENT_ROOT'] . '/data';
$rawFiles = array_diff(scandir($path), array('.', '..'));
for($i = 2; $i < count($rawFiles) + 2; $i++) {
    $files[] =  array('data' => $rawFiles[$i]);
}
echo json_encode($files);