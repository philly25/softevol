<?php

function generateRandomString($length = 10) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}

$formdata = array(
    'name' => $_POST['name'],
    'email' => $_POST['email'],
    'telephone' => $_POST['telephone'],
    'address' => $_POST['address'],
    'street' => $_POST['street'],
    'city' => $_POST['city'],
    'state' => $_POST['state'],
    'zip' => $_POST['zip']
);
$json = json_encode($formdata);
//$action = (isset($_POST['update']))

$fp = fopen($_SERVER['DOCUMENT_ROOT'] . '/data/' . generateRandomString() . '.json', 'w');
fwrite($fp, $json);
fclose($fp);

//echo $json;
echo json_encode(array('response' => true));
