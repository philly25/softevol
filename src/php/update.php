<?php
$filename = $_POST['filename'];
$formdata = $_POST['formdata'];
$customer = array();
parse_str($formdata, $customer);
$json = json_encode($customer);

$fp = fopen($_SERVER['DOCUMENT_ROOT'] . '/data/' . $filename, 'w');
fwrite($fp, $json);
fclose($fp);

echo json_encode(array('response' => true));
