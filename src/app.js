 var blocks = {
     list: '#customers-list',
     add: '#add-customer',
     update: '#update-customer',
     form: '#add-form',
     updateForm: '#update-form'
 };
 $(document).ready(function() {
     // submit add form handler
     $(blocks.form).submit(function(e) {
         e.preventDefault();
         // send($(this), '');
         $.post('php/add.php', $(this).serialize())
             .done(function(data) {
                 $('#add-form').trigger('reset');
             });
     });
 });
