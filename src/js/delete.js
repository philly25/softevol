/**
 * Created by toxa on 23.08.2016.
 */
function remove(id) {
    $.post('php/delete.php', {filename: id + '.json'})
        .done(function(response) {
            $('[data-id="' + id + '"]').fadeOut(100);
        });
}