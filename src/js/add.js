function customerForm() {
    if ($(blocks.add).hasClass('hidden')) {
        $(blocks.add).toggleClass('hidden');
    }
    if (!$(blocks.update).hasClass('hidden')) {
        $(blocks.update).toggleClass('hidden');
    }
    if (!$(blocks.list).hasClass('hidden')) {
        $(blocks.list).toggleClass('hidden');
    }
    $('#add-form').attr('action', 'add');
}
