'use strict';
// (function($){
    var list = (function() {
        var self = this;
        var loaded = false;
        var customers = {};

        var getCustomer = function(id) {
            return customers[id];
        };
        var update = function() {
            $(blocks.list + ' tbody').html('');
            if (!$(blocks.add).hasClass('hidden')) {
                $(blocks.add).toggleClass('hidden');
            }
            if (!$(blocks.update).hasClass('hidden')) {
                $(blocks.update).toggleClass('hidden');
            }
            create();
        };
        var create = function() {
            $(blocks.list).removeClass('hidden');
            $.get('php/data.php')
                .done(function(data) {
                    var json = $.parseJSON(data);
                    $.each(json, function(i, value) {
                        renderCustomer(value.data);
                    });
                    loaded = true;
                });
        };
        var renderCustomer = function (fileName) {
            var id = fileName.split('.');
            $.getJSON('data/' + fileName)
                .done(function(data) {
                    var tbody = $(blocks.list + ' tbody');
                    customers[id] = data;
                    tbody.append('<tr data-id="' + id[0] + '"></tr>');
                    $.each(data, function(key, value) {
                        $(blocks.list + ' tbody tr:last').append('<td>' + value + '</td>');
                    });
                    $(blocks.list + ' tbody tr:last').append('<td class="warning customer-edit" onclick="edit(\'' + id[0] + '\')"><span class="borderless glyphicon glyphicon-edit"></span></td>');
                    $(blocks.list + ' tbody tr:last').append('<td class="danger customer-delete" onclick="remove(\'' + id[0] + '\')"><span class="borderless glyphicon glyphicon-remove"></span></td>');
                })
                .fail(function(jqxhr, textStatus, error) {
                    console.log( "Request Failed: " + error );
                });
            return false;
        };

        return {
            renderCustomer: renderCustomer,
            getCustomer: getCustomer,
            update: update,
            create: create
        }
    })();
// })(jQuery);