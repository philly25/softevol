function edit(id) {
    $.getJSON('data/' + id + '.json')
        .done(function(data) {
            for (var i in data) {
                $('#update-form input[name="'+i+'"]').val(data[i]);
            }

            $(blocks.update).toggleClass('hidden');
            if(!$(blocks.list).hasClass('hidden')) {
                $(blocks.list).addClass('hidden');
            }
            $(blocks.updateForm).submit(function(e) {
                var inputs = $(this).serialize();
                e.preventDefault();
                update(inputs, id + '.json');
            });

        });
}

function update(data, filename) {
    $.post('php/update.php', {
        formdata: data,
        filename: filename
    })
        .done(function(response) {
            console.log(response);
        });
}
